import React, { Component } from "react";
import { withRouter, Route, Redirect, NavLink, Switch } from "react-router-dom";
import sortArr from "../utils/arraySort";
import AsyncComponent from "../AsyncComponent";
import "../styles/index.css";


class Tabs extends Component {
  componentDidMount() {
    // вот здесь может быть запрос к серверу на получение данных
    // но сейчас просто передадим через пропсы
    // почему так? я бы не рендерил приложение, до получения  минимума необходимых нам файлов
    const { tabs } = this.props;
    const sortedArr = tabs.sort(sortArr);
    const tabLinks = [];
    const tabContent = [];
    const ClearRoute = ()=>(
        <Route render={()=><Redirect to={`/${sortedArr[0]['id']}`}/>}/>
    )
    sortedArr.forEach(item => {
      tabLinks.push(
        <NavLink to={`/${item.id}`} key={item.id} activeClassName="selected">
          {item.title}
        </NavLink>
      );
      tabContent.push(
        <Route
          key={item.id}
          path={`/${item.id}`}
          exact={true}
          component={() => (
            <AsyncComponent moduleProvider={() => import(`../${item.path}`)} />
          )}
        />
      );
    });

    tabContent.push(<ClearRoute key={0}/>);

    this.setState({
      tabLinks,
      tabContent
    });
  }
  state = {};

  render() {
    return (
      <div className="tab-container">
        <div className="tab-header">{this.state.tabLinks}</div>
        <div className="tab-content">
        <Switch>
        {this.state.tabContent}
       
        </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(Tabs);
