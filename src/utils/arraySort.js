// сортируем массив по порядкову номеру

const sortArr = (obj1,obj2)=>{
    if(obj1.order>obj2.order){
        return 1;
    }
    if(obj1.order<obj2.order){
        return -1;
    }
    return 0;   
};

export default sortArr;