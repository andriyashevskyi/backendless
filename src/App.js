import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink, withRouter } from "react-router-dom";
import AsyncComponent from "./AsyncComponent";
import sortArr from "./utils/arraySort";
import Tabs from "./components/Tabs";


// в компоненте Tabs написанно почему тяну данные через пропсы
let tabs = [
  {id: 'dummyTable', title: 'Dummy Table', order: 1, path: 'tabs/dummyTable.js'},
  {id: 'dummyChart', title: 'Dummy Chart', order: 2, path: 'tabs/dummyChart.js'},
  {id: 'dummyList', title: 'Dummy List', order: 0, path: 'tabs/dummyList.js'}
];

class App extends Component {
  render() {
    return (    
        <Router>
            <Tabs tabs={tabs}/> 
        </Router> 
    );
  }
}

export default App;
