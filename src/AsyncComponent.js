import React, { Component, PureComponent } from "react";
import { withRouter } from "react-router-dom";

class AsyncComponent extends Component {

  componentDidMount() {
    if (!this.state.Component) {
      this.props.moduleProvider().then(module => {
        this.setState({ Component: module.default});
      });
    }
  }

  state = {
    Component: null
  };


  render() {
    const { Component } = this.state;
    return <div>{Component ? <Component /> : <div>Preloader</div>}</div>;
  }
}

export default AsyncComponent;
